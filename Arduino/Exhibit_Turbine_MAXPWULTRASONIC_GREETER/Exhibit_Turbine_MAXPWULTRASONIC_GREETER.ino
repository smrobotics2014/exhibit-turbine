// Exhibit_Turbine_MAXPWULTRASONIC_GREETER.ino by the St. Mark's School of Texas Robotics Team
// For use with the SM Robotics Team 2014 BEST Exhibit. This Arduino sketch utilizes the ultrasonic sensor.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const int ultraPin = 5;       		   	// Set pin and variable numbers
const int turbineRotateButn = 4;
const int startButtonPin = 7;
const int TimerLEDpin = 11;
const int startButtonLEDpin = 12;
const int UltraONledPin = 13;

int startButtonPress = 0;               // State of press of startButton (boolean - 1/0)
int turbineRotateButnPress = 0;         // State of press of turbine rotation button (boolean - 1/0)
int blocked = 0;
int gameOn = 0;
int greeter = 0;
int greeterOn = 0;
long ultraDistanceRAW;
long ultraDistance;

void setup() {
  Serial.begin(115200);                 // Set up serial at 115200 baud (very fast)

  pinMode(ultraPin, INPUT);     	    // Set digital pin types
  pinMode(startButtonPin, INPUT);
  pinMode(turbineRotateButn, INPUT);
  pinMode(startButtonLEDpin, OUTPUT);
  pinMode(UltraONledPin, OUTPUT);
  pinMode(TimerLEDpin, OUTPUT);
  digitalWrite(TimerLEDpin, LOW);
}

void loop() {
  if(digitalRead(startButtonPin) == HIGH) {
    if(startButtonPress == 0) {
      Serial.print("s1\n");
      digitalWrite(startButtonLEDpin, HIGH);
    }
    startButtonPress = 1;
  }
  else {
    digitalWrite(startButtonLEDpin, LOW);
    startButtonPress = 0;
  }
  
  ultraDistanceRAW = pulseIn(ultraPin, HIGH);
  ultraDistance = ultraDistance/58;			// Convert to CM
  
  if(ultraDistance <= 8) {
    if(blocked != 1) {
      Serial.print("1\n");
      digitalWrite(UltraONledPin, HIGH);
    }
    blocked = 1;
  }
  else {
    if(blocked != 0) {
      Serial.print("0\n");
      digitalWrite(UltraONledPin, LOW);
    }
    blocked = 0;
  }
  
  if (gameOn == 0) {
    if (ultraDistance <= 61 && ultraDistance >= 60) {
      if (greeterOn != 1) {
        Serial.print("g1\n");
      }
      greeterOn = 1;
    }
    else {
      greeterOn = 0;
    }
  }
  
  if(digitalRead(turbineRotateButn) == HIGH) {
    if(turbineRotateButnPress != 1) {
      Serial.print("1\n");
    }
    turbineRotateButnPress = 1;
  }
  else {
    if(turbineRotateButnPress != 0) {
      Serial.print("0\n");
    }
    turbineRotateButnPress = 0;
  }
}

/*
  SerialEvent occurs whenever a new data comes in the
 hardware serial RX.  This routine is run between each
 time loop() runs, so using delay inside loop can delay
 response.  Multiple bytes of data may be available.
 */
void serialEvent() {
  while (Serial.available()) {             // While there is a serial byte
    char rec = Serial.read();              //  Set byte to var
    if (rec == '1') {                      //  If byte is '1't
      digitalWrite(TimerLEDpin, HIGH);     //   Turn on LED
      gameOn = 1;
    }                                      //  End if
    else if (rec == '0') {                 //  If byte is '0'
      digitalWrite(TimerLEDpin, LOW);      //   Turn off LED
      gameOn = 0;
    }                                      //  End if
  }                                        // End while
}
