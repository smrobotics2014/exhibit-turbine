SM Robotics 2014 Turbine Exhibit
================================
---
This project provides the code and resources for the interactive turbine for the [St. Mark's School of Texas](https://www.smtexas.org/home) Robotics Team Texas BEST 2014 Exhibit.

## Requirements
 - An operating system that can run [`node-serialport`](https://github.com/voodootikigod/node-serialport). This includes Win32, Win64, most Linux distributions both 32-bit and 64-bit, and Mac OSX Intel.
 - [NodeJs](http://nodejs.org/) and [NPM](http://npmjs.org/)
 - A webkit-based browser that can run fullsceen (such as [Opera](http://www.opera.com/)), [Chromium](http://www.chromium.org/Home), or [Google Chrome](https://www.google.com/chrome).
 - An [Arduino](http://arduino.cc/) and it peripherals as shown on the Eagle schematic in the Resources folder (ultrasonic sensor, LEDS, resistors, etc.)
 - Command Prompt/Terminal

## Arduino
It is simplest to use the lightweight [Arduino IDE](http://arduino.cc/en/Main/Software#toc2) to compile and upload the program to the Arduino. It is preferred that the Arduino is connected to the computer via USB, as this provides power and serial without complication or risk of damaging the Arduino. We used an Arduino Mega, but an Uno word serve fine as well.

You must selected which program to use. All of these are found in the `Arduino` folder in the root folder:

 - `Exhibit_Turbine_BUTTON` - Use this if you plan to use a button instead of the ultrasonic sensor for detecting turbine movement. This is great for testing without the actual turbine.
 - `Exhibit_Turbine_ULTRASONIC` - Use this if you would like to use the ultrasonic sensor to detect blade movement.
 - `Exhibit_Turbine_ULTRASONIC_GREETER` - Use this if you would like to use the ultrasonic sensor also to detect when a person approaches.
 - `Exhibit_Turbine_ULTRASONIC_GREETERSEPARATE` - Use this if you would like to use a separate the ultrasonic sensor to detect when a person approaches. This allow for separate detection of a person and the blades.


## Computer
You must install the dependencies for the project prior to use:

```bash
$ npm install
```

You may now run the program:

```bash
$ node ./exhibit.js
```

For information on its run options, run it with the `--help` flag. By default, you should now be able to open your browser to either `http://127.0.0.1:8080/` or `http://localhost:8080/`. You can change the defaults by editing the first JSON object in `Exhibit.js`. The options and their functions are labelled. **The program will not start if an invalid serial port is specified.**

Clicking anywhere on-screen in the browser will open a debug menu that can be used to simulate events and change various options. You can click again anywhere on-screen (other than the menu itself) to hide the menu. In the menu there is a button labelled "Open Remote." This will open a separate, touchscreen-friendly window that can also be used to control the system.

## License
README.md by the St. Mark's School of Texas Robotics Team
For use with the SM Robotics Team 2014 BEST Exhibit.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.