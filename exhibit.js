// exhibit.js by the St. Mark's School of Texas Robotics Team
// For use with the SM Robotics Team 2014 BEST Exhibit. This is the main file.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

var defaults = {															// Start Config
	serial: {																//  Serial Settings Non-configurable serial settings are Arduino defaults
		port: "COM1",														//   Set serial port
		baudRate: 115200,													//   Set serial baudrate
		parser: "\n"														//   Set serial newline character
	},																		//  End Serial Settings
	btnSpam: {
		msg: "Dude, stop pressing the button!"
	},
	commands: {		  														//  Arduino Settings
		on: "1",															//   Set on command
		off: "0",															//   Set off command
		startTimer: "s1",													//	 Set start timer command
		greeter: "g1",														//	 Set greeter command
		rotation: 3 														//	 Set Distance for one on/off
	},																		//  End  Arduino Settings
	turbine: {																//  Start Turbine Config
		energy: 0.56,														//    Energy produced in one blade detection
		enableMinSpeed: false,	// Note: The lower the speed, the faster	//    Enable or requiring to a minimum speed
		enableMaxSpeed: false,	//       the blades are spinning. If the	//    Enable or restricting to a maximum speed
		minSpeed: 75,			//       blades are too fast, speed	the		//    Minimum speed for blade detection (ms)
		maxSpeed: 750,			//       will be lower than minSpeed.		//    Maximum speed for blade detection (ms)
		enableDebounce: true,												//    More accurate than min/max. Always not (not just when timer on).
		debounceRate: 75													//    Debounce rate (ms)
	},																		//  End Turbine Config
	timer: {																//  Start Timer Settings
		time: 7.500,														//   Set timer (seconds)
		options: {															//   Start Timer Library Settings
			refreshRateMS: 5 ,     											//    How often the clock should be updated
			almostDoneMS: 10000   											//    When counting down - this event will fire with this many milliseconds remaining on the clock
		}																	//   End Timer Library Settings
	},																		//  End Timer Settings
	greeter: {																//  Start Greeter Settings
																			//   Nothing here
	},																		//  End Greeter Settings
	server: {																//  Start Server Settings
		web: {																//   Set web server
			dir: './web',													//    Set directory for server
			cacheAge: 86400000,												//    Set maximum cache time (minutes)
			port: 8080														//    Set server port
		},																	//   End web server
		primus: {															//   Start Primus (websockets) settings
			parser: 'JSON',													//    Set parser for recieved data. Default: 'JSON'
			pathname: '/ws',												//    Set path on server to host websockets from
			transformer: 'websockets',										//    Set library to serve with
			clientLibPath: './web/assets/js/primus.js'						//    Set path to client library
		}																	//   End Primus (websockets) settings
	},																		//  End Server Settings
	leaderboard: {															//  Start Leaderboard Settings
		file: "./leaderboard.json",											//   Set leaderboard file location and name
		data: [																//   Set default data (three places)
			{watts: 0, rotations: 0, fullRotations: 0},						//    Set default data for 1st place
			{watts: 0, rotations: 0, fullRotations: 0},						//    Set default data for 2nd place
			{watts: 0, rotations: 0, fullRotations: 0}						//    Set default data for 3rd place
		]																	//   End default data (three places)
	}																		//  End Leaderboard Settings
};																			// End Config
exports.defaults = defaults;

var program = require('commander');
program
	.version(require("./package.json").version)
	.option('-c, --com [port]', 'Set serial port. Default: ' + defaults.serial.port)
	.option('-b, --baudrate [baud]', 'Set serial baudrate. Default: ' + defaults.serial.baudRate)
	.option('-t, --time [seconds]', 'Set time. Default: ' + defaults.timer.time)
	.option('-p, --port [port]', 'Set server port. Default: ' + defaults.server.web.port)
	.option('-l, --leaderboard [path]', 'Set path to leaderboard file. Default: ' + defaults.leaderboard.file)
	.option('-e, --energy [watts]', 'Set watts produced per full rotation. Default: ' + defaults.turbine.energy)
	.option('-m, --minspeed [milliseconds]', 'Set minimum time between blades to count as rotation. Default: ' + defaults.turbine.minSpeed)
	.option('-a, --maxspeed [milliseconds]', 'Set maximum time between blades to count as rotation. Default: ' + defaults.turbine.maxSpeed)
	.option('-r, --rotation [x]', 'Set number of blades. Default: ' + defaults.commands.rotation)
	.parse(process.argv);
if (program.com) defaults.serial.port = program.com;
if (program.baudrate) defaults.serial.baudRate = program.baudrate;
if (program.time) {
	if (program.time != 0) {
		defaults.timer.time = program.time;
	}
	else {
		throw "Timer cannot be set to 0!"
	}
}
if (program.port) defaults.server.web.port = program.port;
if (program.leaderboard) defaults.leaderboard.file = program.leaderboard;
if (program.energy) defaults.turbine.energy = program.energy;
if (program.minspeed) defaults.turbine.minSpeed = program.minspeed;
if (program.maxspeed) defaults.turbine.maxSpeed = program.maxspeed;
if (program.rotation) defaults.commands.rotation = program.rotation;

var async = require("async"),												// Load libraries
	exec = require('child_process').exec,
	fs = require('fs'),
	npid = require('npid'),
	//NanoTimer = require("nanotimer"),
	Stopwatch = require('timer-stopwatch'),
	debounce = require('debounce'),
	serialport = require("serialport"),
	SerialPort = serialport.SerialPort,
	server = require("./lib/exhibitServer.js"),
	scrn = require("./lib/TUIscreen.js"),
	leaderboard = require("./lib/leaderboard.js");

try {
	fs.exists('./exhibit.pid', function(exists) {
		if (exists) {
			fs.unlink('./exhibit.pid', function(err) {
				if (err) throw err;
				var pid = npid.create('./exhibit.pid');
				pid.removeOnExit();
			});
		}
		else {
			var pid = npid.create('./exhibit.pid');
			pid.removeOnExit();
		}
	});
}
catch (err) {
    throw err;
}

scrn.start();																// Start TUI

var calibrating = false,													// Set vars
	sequenceOn = false,
	timerHandle,
	timer,
	//timeCount,
	rotation = false,
	rotations = 0,
	distance = 0,
	distancePerSec = 0,
	fullRotation = 0,
	rotationWatts = 0,
	btnSpam = 0,
	btnSpamMsg = false,
	rotationTimes = [];
	rotationSpeeds = [];

var arduino = new SerialPort(defaults.serial.port, {						// Configure serial port
    baudRate : defaults.serial.baudRate,
    dataBits : 8,
    parity : 'none',
    stopBits: 1,
	parser: serialport.parsers.readline(defaults.serial.parser),
	flowControl : false,
});

function log(data) {
	scrn.log(data);
}
module.exports.log = log;

function primusConnect(spark) {
	spark.write({timer:{set:defaults.timer.time},rotation:{distance: defaults.commands.rotation, rotations: 0},user:{distance: 0, rotationWatts: 0},turbine: defaults.turbine});
	leaderboard.getLeaderboard(function(data) {
		server.primus.write({leaderboard: {data: data}});
	});
	spark.on('data', function (data) {
		scrn.log("Received: " + JSON.stringify(data));
		if (data.hasOwnProperty('timer')) {
			if (data.timer == "start") { startTimerSafe();}
		}
		else if (data.hasOwnProperty('turbine')) {
			if (data.turbine == "rotation") { rotate(); }
		}
		else if (data.hasOwnProperty('getSpeeds')) {
			if (data.getSpeeds == true) { spark.write({turbine: defaults.turbine}); }
		}
		else if (data.hasOwnProperty('minSpeed')) {
			if (data.minSpeed == "toggle") {
				defaults.turbine.enableMinSpeed = !defaults.turbine.enableMinSpeed;
				server.primus.write({turbine: defaults.turbine});
			}
		}
		else if (data.hasOwnProperty('maxSpeed')) {
			if (data.maxSpeed == "toggle") {
				defaults.turbine.enableMaxSpeed = !defaults.turbine.enableMaxSpeed;
				server.primus.write({turbine: defaults.turbine});
			}
		}
		else if (data.hasOwnProperty('debounce')) {
			if (data.debounce == "toggle") {
				defaults.turbine.enableDebounce = !defaults.turbine.enableDebounce;
				server.primus.write({turbine: defaults.turbine});
			}
		}
		else if (data.hasOwnProperty('leaderboard')) {
			if (data.leaderboard == "reset") {
				leaderboard.reset(function(leaderboardData) {
					server.primus.write({leaderboard: {data: leaderboardData}});
				});
			}
		}
	});
};
module.exports.primusConnect = primusConnect;

function startTimer() {														// Function for when timer starts
	rotationTimes = [];
	rotationSpeeds = [];
	btnSpam = 0;
	btnSpamMsg = false;
	rotations = 0;
	distance = 0;
	rotationWatts = 0;
	timer = new Stopwatch(defaults.timer.time*1000, defaults.timer.options);
	timer.on('time', function(){updateTime();});
	timer.on('done', function(){timeOut();});
	timer.start();
	sequenceOn = true;														// Set var for timer running to true
	//server.primus.write({timer: {run: "start", time: timer.ms}});
	arduino.write("1", function(err, results) {if (err) throw err;});		// Tell Arduino to turn on LED
}
module.exports.startTimer = startTimer;

function startTimerSafe() {
	if (sequenceOn != true) {												// If the timer isn't already running
		startTimer();														// Start timer
	}
}
module.exports.startTimerSafe = startTimerSafe;

function updateTime() {														// Show timer updates
	if (timer.ms == 0) {
		server.primus.write({timer: {run: "stop", time: timer.ms/1000}});
	}
	else {
		server.primus.write({timer: {run: "start", time: timer.ms/1000}});
	}
	//console.log((timeCount > 0 ? timeCount + " seconds left" : "Time over"));	// Log to console
	scrn.timer(timer.ms/1000); 												// Log to console
}
function timeOut() {														// At timer finish
	sequenceOn = false;														// Set var for timer running to false
	arduino.write("0", function(err, results) {if (err) throw err;});		// Tell Arduino to turn off LED
	scrn.log("1/" + defaults.commands.rotation + " rotations in " + defaults.timer.time + " seconds: " + rotations);			// Log rotations to console
	//scrn.log("Times: " + JSON.stringify(rotationTimes));			// Log rotations to console
	scrn.log("Speeds: " + JSON.stringify(rotationSpeeds));			// Log rotations to console
	server.primus.write({timer: {run: "stop", time: 0.000}, rotation: {rotations: rotations, distance: defaults.commands.rotation}, user: {distance: distance, rotationWatts: rotationWatts}});
	leaderboard.updateLeaderboard({rotations: rotations, fullRotations: distance, watts: rotationWatts}, function(data, changed, place, tie) {
		server.primus.write({leaderboard: {data: data, changed: changed, place: place, tie: tie}});
	});
}

function greeterHello() {
	server.primus.write({greeter: {sayHello: true}});
}
module.exports.greeterHello = greeterHello;

function rotate() {
	if (sequenceOn == true) {												// If timer running
		if (rotationTimes.length > 0) {
			rotationTimes.push(timer.ms);
			rotationSpeeds.push(rotationTimes[rotationTimes.length-2] - rotationTimes[rotationTimes.length-1]);
			next();
		}
		else {
			rotationTimes.push(timer.ms);
			rotationSpeeds.push(defaults.timer.time*1000 - rotationTimes[0]);
			next();
		}
		function next() {
			if (rotationSpeeds[rotationSpeeds.length-1] < defaults.turbine.minSpeed && defaults.turbine.enableMinSpeed == true) { 
				scrn.log ("Too fast!");
				//server.primus.write({notify: {tooFast: true}});
			}
			else if (rotationSpeeds[rotationSpeeds.length-1] > defaults.turbine.maxSpeed && defaults.turbine.enableMaxSpeed == true) {
				scrn.log ("Too slow!");
				//server.primus.write({notify: {tooSlow: true}});
			}
			else {
				rotations++;												// Add one to rotations count
				scrn.log("Rotated 1/" + defaults.commands.rotation + " (" + rotations + ") " + 1 + " time.");	// Log to console
				distance = rotations / defaults.commands.rotation;
				rotationWatts = distance * defaults.turbine.energy;
				server.primus.write({notify: {tooSlow: false, tooFast: false}});
				server.primus.write({rotation: {rotations: rotations, distance: defaults.commands.rotation}, user: {distance: distance, rotationWatts: rotationWatts}});
			}
			server.primus.write({rotation: {rotationNow: true, rotationNowNumber: 1, distance: defaults.commands.rotation, rotationTime: rotationSpeeds[rotationSpeeds.length-1]}});
		}
	}
	else {
		server.primus.write({rotation: {rotationNow: true, rotationNowNumber: 1, distance: defaults.commands.rotation, rotationTime: 1000}});
	}
}

arduino.on("open", function (err) {											// Open Arduino serial port
	if (err) throw err;
	scrn.log("Serial: Listening on " + defaults.serial.port + " at " + defaults.serial.baudRate + " baud.")
	//arduino.flush(function (err) {										  // Flush serial port
		if (err) throw err;
		arduino.on('data', function(data) {									// When data arrives
			// if (data == "Calibration started") {							// If Arduino Calibrating
				// scrn.log("Calibrating");
				// server.primus.write({arduino: {start: "calibrate"}});
				// calibrating = true;
			// }
			// if (data == "Calibration finished") {						// If Arduino Finished Calibrating
				// scrn.log("Calibrating Finished");
				// server.primus.write({arduino: {stop: "calibrate"}});
				// calibrating = false;
			// }
			if (data == defaults.commands.on) {								// If data meets threshold
				if (rotation != true) {										// If not already in the middle of full rotation
					if (defaults.turbine.enableDebounce == true) {
						debounce(rotate, defaults.turbine.debounceRate);
					}
					else {
						rotate();
					}
					scrn.sensor(data, true);
				}
				rotation = true;											// Set rotation to on
			}
			else if (data == defaults.commands.off){
				rotation = false;											// Set rotation to off
				scrn.sensor(data, false);
			}
			else if (data == defaults.commands.startTimer) {				// When the start button is pressed
				startTimerSafe();											// Start timer
				btnSpam++;
				if (btnSpam >= 10) {
					btnSpamMsg = true;
					scrn.log("Button Spam: " + defaults.btnSpam.msg);
					server.primus.write({btnSpam: {on: true, msg: defaults.btnSpam.msg}});
				}
			}
			else if (data == defaults.commands.greeter) {					// When a person approaches
				greeterHello();
			}
		});
	//});
});

server.start(function(){
	scrn.log('Server: Listening on http://localhost:' + defaults.server.web.port);	// Log to console
});