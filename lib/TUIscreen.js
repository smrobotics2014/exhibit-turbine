// TUIscreen.js by the St. Mark's School of Texas Robotics Team
// For use with the SM Robotics Team 2014 BEST Exhibit. This module creates a nicer TUI.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

var blessed = require('blessed'),										// Load libraries
	moment = require('moment'),
	parent = module.parent.exports;

var screen = blessed.screen(),
	started = false,
	mainBox,
	titleBox,
	timerBox,
	timerBoxOn,
	timerBoxOff,
	timerRun = false,
	sensorBox,
	sensorBoxOn,
	sensorBoxOff,
	sensorRun = false;
exports.screen = screen;
exports.started = started;
exports.mainBox = mainBox;
exports.titleBox = titleBox;
exports.timerBox = timerBox;
exports.timerBoxOn = timerBoxOn;
exports.timerBoxOff = timerBoxOff;
exports.timerRun = timerRun;
exports.sensorBox = sensorBox;
exports.sensorBoxOn = sensorBoxOn;
exports.sensorBoxOff = sensorBoxOff;
exports.sensorRun = sensorRun;

function start() {
	titleBox = blessed.text({
		top: '0',
		bottom: 'bottom',
		left: 'left',
		width: '100%',
		height: '2%',
		content: 'SM Robotics BEST 2014 Exhibit - Interactive Turbine',
		tags: true,
		align: 'center',
		style: {
			fg: 'blue',
			bg: 'yellow'
		}
	});
	screen.append(titleBox);
	mainBox = blessed.text({
		top: '3%',
		bottom: 'bottom',
		left: 'left',
		width: '100%',
		height: '94%',
		content: 'Press "s" or Enter to start the timer (no Arduino required). You may also click anywhere in the web app to open a remote that can start the timer and add rotations. Use PgUp and PgDown to scroll the log. Use Home to jump to the top and End to jump to the bottom.',
		tags: true,
		border: {
			type: 'line'
		},
		style: {
			fg: 'white',
			bg: 'blue',
			border: {
				fg: '#ccc'
			}
		},
		scrollable: true
	});
	screen.append(mainBox);
	timerBoxOff = blessed.text({
		top: '96%',
		bottom: 'bottom',
		left: 'left',
		width: '50%',
		height: '1%',
		content: ' Seconds Left: 00.000',
		tags: true,
		style: {
			fg: 'white',
			bg: 'red',
		}
	});
	timerBoxOn = blessed.text({
		top: '96%',
		bottom: 'bottom',
		left: 'left',
		width: '50%',
		height: '1%',
		content: ' Seconds Left: 00.000',
		tags: true,
		style: {
			fg: 'white',
			bg: 'green',
		}
	});
	timerBox = timerBoxOff;
	timerRun = false;
	screen.append(timerBox);
	sensorBoxOff = blessed.text({
		top: '96%',
		bottom: 'bottom',
		left: '50%',
		width: '50%',
		height: '1%',
		content: 'Sensor: 0 ',
		tags: true,
		align: 'right',
		style: {
			fg: 'white',
			bg: 'red',
		}
	});
	sensorBoxOn = blessed.text({
		top: '96%',
		bottom: 'bottom',
		left: '50%',
		width: '50%',
		height: '1%',
		content: 'Sensor: 0 ',
		tags: true,
		align: 'right',
		style: {
			fg: 'white',
			bg: 'green',
		}
	});
	sensorBox = sensorBoxOff;
	sensorRun = false;
	screen.append(sensorBox);
	screen.key(['C-c'], function(ch, key) {						// Quit on Control-C
		return process.exit(0);
	});
	
	screen.key(['s', 'enter'], function(ch, key) {				// Start Timer
		parent.startTimerSafe();
	});
	
	screen.key(['g', 'h'], function(ch, key) {					// Say Hello
		parent.greeterHello();
	});
	
	screen.key(['pagedown'], function(ch, key) {				// Page Down, Scroll Down
		mainBox.scroll(1);
		screen.render();
	});
	screen.key(['pageup'], function(ch, key) {					// Page Up, Scroll Up
		mainBox.scroll(-1);
		screen.render();
	});
	screen.key(['home'], function(ch, key) {					// Home, Scroll to Top
		mainBox.scrollTo(0);
		screen.render();
	});
	screen.key(['end'], function(ch, key) {						// End, Scroll to Bottom
		mainBox.scroll(Number.MAX_VALUE);
		screen.render();
	});
	screen.render();
	started = true;
}
exports.start = start;

function log(msg) {
	if (started == true) {
		mainBox.insertBottom(msg);
		mainBox.scroll(Number.MAX_VALUE);
		screen.render();
	}
	else {
		console.log("Screen: Not started!");
	}
}
exports.log = log;

function timer(seconds) {
	if (started == true) {
		if (seconds != 0) {
			if (timerRun == false) {
				screen.remove(timerBox);
				timerBox = timerBoxOn;
				screen.append(timerBox);
			}
			timerBox.setContent(" Seconds Left: " + moment(seconds, "s.SSS").format("ss.SSS"));
			screen.render();
			timerRun = true;
		}
		else {
			if (timerRun == true) {
				screen.remove(timerBox);
				timerBox = timerBoxOff;
				screen.append(timerBox);
			}
			timerBox.setContent(" Seconds Left: 00.000");
			screen.render();
			timerRun = false;
		}
	}
	else {
		console.log("Screen: Not started!");
	}
}
exports.timer = timer;

function sensor(value, on) {
	if (started == true) {
		if (on == true) {
			if (timerRun == false) {
				screen.remove(sensorBox);
				sensorBox = sensorBoxOn;
				screen.append(sensorBox);
			}
			sensorBox.setContent("Sensor: " + value + " ");
			screen.render();
			sensorRun = true;
		}
		else {
			if (sensorRun == true) {
				screen.remove(sensorBox);
				sensorBox = sensorBoxOff;
				screen.append(sensorBox);
			}
			sensorBox.setContent("Sensor: " + value + " ");
			screen.render();
			sensorRun = false;
		}
	}
	else {
		console.log("Screen: Not started!");
	}
}
exports.sensor = sensor;