// ExhibitServer.js by the St. Mark's School of Texas Robotics Team
// For use with the SM Robotics Team 2014 BEST Exhibit. This module acts as a WebSockets and HTTP server.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

var compression = require('compression'),										// Load libraries
	fs = require('fs'),
	http = require('http'),
	express = require('express'),
	Primus = require("primus"),
	parent = module.parent.exports;

var defaults = parent.defaults.server;
exports.defaults = defaults;

var app = express();														// Set up Express Web Framework

app.use(compression());														// Use gzip compression
app.use(express.static(defaults.web.dir, { maxAge: defaults.web.cacheAge }));	// Set Express server directory

var server = http.createServer(app),										// Transfer Express to HTTP server module
	primus = new Primus(server, {											// Set up Primus (for Websockets)
		parser: defaults.primus.parser,
		pathname: defaults.primus.pathname,
		transformer: defaults.primus.transformer							// Use 'ws' library transformer - fastest, most reliable, and in controlled environment (no fallback protocols are needed)
	});
exports.server = server;
exports.primus = primus;

primus.save(defaults.primus.clientLibPath);

function start(callback) {
	server.listen(defaults.web.port, function listening() {					// Start server
		callback();
	});
}
exports.start = start;

primus.on('connection', function (spark) {
	parent.log("Primus: Connected to " + spark.address.ip + ":" + spark.address.port + ". ID: " + spark.id);
	parent.primusConnect(spark);
});
primus.on('disconnect', function (spark) {
	parent.log("Primus: Disconnected from " + spark.address.ip + ":" + spark.address.port + ". ID: " + spark.id);
});