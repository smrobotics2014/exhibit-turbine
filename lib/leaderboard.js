// leaderboard.js by the St. Mark's School of Texas Robotics Team
// For use with the SM Robotics Team 2014 BEST Exhibit. This module creates a leaderboard.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

var async = require('async'),												// Load libraries
	assert = require('assert'),
	fse = require('fs-extra'),
	parent = module.parent.exports;

var data = {};																// Set vars
exports.data = data;

var defaults = parent.defaults.leaderboard;
exports.defaults = defaults;

function deepEqual(a, b) {                                   				 // Function to check if JSON objects are equal, but don't through error; instead use boolean
    try { assert.deepEqual(a, b); }
	catch (err) {
		if (err.name === "AssertionError") { return false; }
		throw err;
    }
    return true;
};

function updateLeaderboard(newData, callback) {								// Function for updating the leaderboard
	checkFile(defaults.file, function() {									//  Ensure that a file is there and get data from it
		dataOld = data;														//   Store the previous data. So that we can use the "changed" boolean
		if (newData.rotations > data[0].rotations) {						//   If beats first place
			data[2] = data[1];												//    2nd place -> 3rd place
			data[1] = data[0];												//    1nd place -> 2nd place
			data[0] = newData;												//    New record -> 1st place
			updateJson(defaults.file, data, function() {					//    Save to JSON file to save leaderboard
				callback(data, deepEqual(dataOld, data), 1, false);			//     Return: New data, If data changed, place, if tie
			});																//    End  Save to JSON file to save leaderboard
		}																	//   End if beats first place
		else if (newData.rotations > data[1].rotations && newData.rotations != data[0].rotations) { //   If beats second place and not equal to first place
			data[2] = data[1];												//    2nd place -> 3rd place
			data[1] = newData;												//    New record -> 2nd place
			updateJson(defaults.file, data, function() {					//    Save to JSON file to save leaderboard
				callback(data, deepEqual(dataOld, data), 2, false);			//     Return: New data, If data changed, place, if tie
			});																//    End  Save to JSON file to save leaderboard
		}																	//   End if beats first place
		else if (newData.rotations > data[2].rotations && newData.rotations != data[0].rotations && newData.rotations != data[1].rotations) { //   If beats third place and not equal to second nor first place
			data[2] = newData;												//    New record -> 3rd place
			updateJson(defaults.file, data, function() {					//    Save to JSON file to save leaderboard
				callback(data, deepEqual(dataOld, data), 3, false);			//     Return: New data, If data changed, place, if tie
			});																//    End  Save to JSON file to save leaderboard
		}																	//   End if beats first place
		else {																//   Otherwise...
			if (deepEqual(newData, data[0])) {								//    If equal to 1st place
				callback(data, false, 1, true);								//     Return: New data, no data change, 1st place, tie
			}																//    End equal to 1st place
			else if (deepEqual(newData, data[1])) {							//    If equal to 2nd place
				callback(data, false, 2, true);								//     Return: New data, no data change, 1st place, tie
			}																//    End equal to 2nd place
			else if (deepEqual(newData, data[2])) {							//    If equal to 3rd place
				callback(data, false, 3, true);								//     Return: New data, no data change, 1st place, tie
			}																//    End equal to 3rd place
			else {															//    If equal to nothing and less than everything
				callback(data, false, false, false);						//     Return: New data, no data change, no place, no tie
			}																//    End equal to nothing and less than everything
		}																	//   End Otherwise
	});																		//  End check for leaderboard file
}																			// End Function
exports.updateLeaderboard = updateLeaderboard;

function checkFile(file, callback) {										// Function checking if the leaderboard file exists & invoking create if not
	fse.exists(file, function(exists) {										//  Check if the file exists
		if (exists) {														//   If it does exist
			fse.readJson(file, function(err, dataJSON) {					//    Get data from it
				if (err) throw err;											//     If there is an error, stop & show it
				data = dataJSON;											//     Set the data
				callback();													//     Done.
			});																//    End get data from it
		}																	//   End if it does exist
		else {																//   If the file doesn't exist
			newFile(file, callback);										//    Start function to create it & done.
		}																	//   End if the file doesn't exist
	});																		//  End check if the file exists
}																			// End function
exports.checkFile = checkFile;

function newFile(file, callback) {											// Function for creating a new leaderboard file
	fse.ensureFile(file, function(err) {									//  Create a file if there isn't one
		if (err) throw err;													//   If there is an error, stop & show it
		fse.outputJson(file, defaults.data, function(err) {					//   Set defaults to file
			if (err) throw err;												//    If there is an error, stop & show it
			data = defaults.data;											//    Set the data
			callback(data);														//    Done.
		});																	//   End set defaults to file
	});																		//  End create a file if there isn't one
}																			// End function
exports.newFile = newFile;

function updateJson(file, dataJSON, callback) {								// Function for setting data to leaderboard file
	fse.outputJson(file, dataJSON, function(err) {							//  Output data in JSON file
		if (err) throw err;													//   If there is an error, stop & show it
		data = dataJSON;													//   Set the data
		callback();															//   Done.
	});																		//  End output data in JSON file
}																			// End function
exports.updateJson = updateJson;

function getLeaderboard(callback) {											// Function for getting the leaderboard without changing anything
	checkFile(defaults.file, function() {									//  Ensure that a file is there and get data from it 
		callback(data);														//   Return data
	});																		//  End check for leaderboard file
}																			// End function
exports.getLeaderboard = getLeaderboard;

function reset(callback) {
	newFile(defaults.file, callback);
}
exports.reset = reset;