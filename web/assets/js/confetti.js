var confettiOn = false,
	confettiCanvas,
	confettiCtx,
	confettiHandler,
	confettiWidth,
	confettiHeight,
	confettiMp = 150, //max confettiParticles
	confettiParticles = [];

$(window).resize(function () {
    confettiCanvas = document.getElementById("confetti");
    //confettiCanvas dimensions
    confettiWidth = window.innerWidth;
    confettiHeight = window.innerHeight;
    confettiCanvas.width = confettiWidth;
    confettiCanvas.height = confettiHeight;
});
$(document).ready(function () {
    confettiCanvas = document.getElementById("confetti");
    confettiCtx = confettiCanvas.getContext("2d");
    //confettiCanvas dimensions
    confettiWidth = window.innerWidth;
    confettiHeight = window.innerHeight;
    confettiCanvas.width = confettiWidth;
    confettiCanvas.height = confettiHeight;

    for (var i = 0; i < confettiMp; i++) {
        confettiParticles.push({
            x: Math.random() * confettiWidth, //x-coordinate
            y: Math.random() * confettiHeight, //y-coordinate
            r: randomFromTo(5, 30), //radius
            d: (Math.random() * confettiMp) + 10, //density
            color: "rgba(" + Math.floor((Math.random() * 255)) + ", " + Math.floor((Math.random() * 255)) + ", " + Math.floor((Math.random() * 255)) + ", 0.7)",
            tilt: Math.floor(Math.random() * 10) - 10,
            tiltAngleIncremental: (Math.random() * 0.07) + .05,
            tiltAngle: 0
        });
    }
});


function draw() {
    confettiCtx.clearRect(0, 0, confettiWidth, confettiHeight);
    for (var i = 0; i < confettiMp; i++) {
        var p = confettiParticles[i];
        confettiCtx.beginPath();
        confettiCtx.lineWidth = p.r / 2;
        confettiCtx.strokeStyle = p.color;  // Green path
        confettiCtx.moveTo(p.x + p.tilt + (p.r / 4), p.y);
        confettiCtx.lineTo(p.x + p.tilt, p.y + p.tilt + (p.r / 4));
        confettiCtx.stroke();  // Draw it
    }

    update();
}
function randomFromTo(from, to) {
    return Math.floor(Math.random() * (to - from + 1) + from);
}
var TiltChangeCountdown = 5;
//Function to move the snowflakes
//angle will be an ongoing incremental flag. Sin and Cos functions will be applied to it to create vertical and horizontal movements of the flakes
var angle = 0;
var tiltAngle = 0;
function update() {
    angle += 0.01;
    tiltAngle += 0.1;
    TiltChangeCountdown--;
    for (var i = 0; i < confettiMp; i++) {
        
        var p = confettiParticles[i];
        p.tiltAngle += p.tiltAngleIncremental;
        //Updating X and Y coordinates
        //We will add 1 to the cos function to prevent negative values which will lead flakes to move upwards
        //Every particle has its own density which can be used to make the downward movement different for each flake
        //Lets make it more random by adding in the radius
        p.y += (Math.cos(angle + p.d) + 1 + p.r / 2) / 2;
        p.x += Math.sin(angle);
        //p.tilt = (Math.cos(p.tiltAngle - (i / 3))) * 15;
        p.tilt = (Math.sin(p.tiltAngle - (i / 3))) * 15;

        //Sending flakes back from the top when it exits
        //Lets make it a bit more organic and let flakes enter from the left and right also.
        if (p.x > confettiWidth + 5 || p.x < -5 || p.y > confettiHeight) {
            if (i % 5 > 0 || i % 2 == 0) //66.67% of the flakes
            {
                confettiParticles[i] = { x: Math.random() * confettiWidth, y: -10, r: p.r, d: p.d, color: p.color, tilt: Math.floor(Math.random() * 10) - 10, tiltAngle: p.tiltAngle, tiltAngleIncremental: p.tiltAngleIncremental };
            }
            else {
                //If the flake is exitting from the right
                if (Math.sin(angle) > 0) {
                    //Enter from the left
                    confettiParticles[i] = { x: -5, y: Math.random() * confettiHeight, r: p.r, d: p.d, color: p.color, tilt: Math.floor(Math.random() * 10) - 10, tiltAngleIncremental: p.tiltAngleIncremental };
                }
                else {
                    //Enter from the right
                    confettiParticles[i] = { x: confettiWidth + 5, y: Math.random() * confettiHeight, r: p.r, d: p.d, color: p.color, tilt: Math.floor(Math.random() * 10) - 10, tiltAngleIncremental: p.tiltAngleIncremental };
                }
            }
        }
    }
}
function StartConfetti() {
	confettiOn = true;
    confettiWidth = window.innerWidth;
    confettiHeight = window.innerHeight;
    confettiCanvas.width = confettiWidth;
    confettiCanvas.height = confettiHeight;
    confettiHandler = setInterval(draw, 15);
}
function StopConfetti() {
	confettiOn = false;
    clearTimeout(confettiHandler);
    if (confettiCtx == undefined) return;
    confettiCtx.clearRect(0, 0, confettiWidth, confettiHeight);
}
function ToggleConfetti() {
	if (confettiOn == true) {
		StopConfetti();
	}
	else {
		StartConfetti();
	}
}
function ToggleConfettiFade() {
	if (confettiOn == true) {
		$('canvas#confetti').stop(true, true).fadeOut(1500, function() {
			StopConfetti();
		});
	}
	else {
		StartConfetti();
		$('canvas#confetti').stop(true, true).fadeIn(1500);
	}
}