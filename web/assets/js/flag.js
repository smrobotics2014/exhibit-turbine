var flagTimer = false, flagH = new Image, flagLoaded = false;
flagH.src = '/assets/img/teamFlag200px.png';
function startFlag() {
	if (flagLoaded == true) {
		loadFlag();
	}
	else {
		flagH.onload = function() {
			flagLoaded = true;
			loadFlag();
		}
	}
}
function stopFlag() {
	clearInterval(flagTimer);
	flagTimer = false;
	flagOn = false;
}
function toggleFlag() {
	if (flagOn == true) {
		stopFlag();
	}
	else {
		startFlag();
	}
}
function toggleFlagFade() {
	if (flagOn == true) {
		$('div#content div#main div#bottomLogos canvas#canvasFlag').stop(true, true).fadeOut(500, function() {
			stopFlag();
		});
	}
	else {
		startFlag();
		$('div#content div#main div#bottomLogos canvas#canvasFlag').stop(true, true).fadeIn(500);
	}
}

function loadFlag() {
	flagOn = true;
	var flag = document.getElementById('canvasFlag');
	var flagAmp = 20;
	flag.width  = flagH.width;
	flag.height = flagH.height + flagAmp*2;
	flag.getContext('2d').drawImage(flagH,0,flagAmp,flagH.width,flagH.height);
	flag.style.marginLeft = -(flag.width/2)+'px';
	flag.style.marginTop  = -(flag.height/2)+'px';
	if (flagTimer != false) {
		clearInterval(flagTimer);
	}
	flagTimer = waveFlag( flag, flagH.width/10, flagAmp);
}

function waveFlag( canvas, wavelength, amplitude, period, shading, squeeze) {
	if (!squeeze)    squeeze    = 0;
	if (!shading)    shading    = 100;
	if (!period)     period     = 350;
	if (!amplitude)  amplitude  = 10;
	if (!wavelength) wavelength = canvas.width/10;

	var fps = 60;
	var ctx = canvas.getContext('2d');
	var   w = canvas.width, h = canvas.height;
	var  od = ctx.getImageData(0,0,w,h).data;
	// var ct = 0, st=new Date;
	return setInterval(function(){
		var id = ctx.getImageData(0,0,w,h);
		var  d = id.data;
		var now = (new Date)/period;
		for (var y=0;y<h;++y){
			var lastO=0,shade=0;
			var sq = (y-h/2)*squeeze;
			for (var x=0;x<w;++x){
				var px  = (y*w + x)*4;
				var pct = x/w;
				var o   = Math.sin(x/wavelength-now)*amplitude*pct;
				var y2  = y + (o+sq*pct)<<0;
				var opx = (y2*w + x)*4;
				shade = (o-lastO)*shading;
				d[px  ] = od[opx  ]+shade;
				d[px+1] = od[opx+1]+shade;
				d[px+2] = od[opx+2]+shade;
				d[px+3] = od[opx+3];
				lastO = o;
			}
		}
		ctx.putImageData(id,0,0);		
		// if ((++ct)%100 == 0) console.log( 1000 * ct / (new Date - st));
	},1000/fps);
}